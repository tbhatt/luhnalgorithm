import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InputTest {
    Input input = new Input();
    @Test
    void getAlgorithm() {

    }

    @Test
    void convertToArray() {
        List<Integer> digits15 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,0,1,2,3,4,5));
        List<Integer> digits5 = new ArrayList<>(Arrays.asList(1,2,3,4,5));

        assertEquals(digits15, input.convertToArray("123456789012345"));
        assertEquals(digits5, input.convertToArray("12345"));
    }
}