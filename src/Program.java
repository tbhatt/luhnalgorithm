import java.util.List;

public class Program {
    public static void main(String[] args) {
        LuhnAlgorithm luhn = new LuhnAlgorithm();
        Input input = new Input();

        List<Integer> numbers = input.getAlgorithm(); //Read input digits
        int size = numbers.size();
        int provided = numbers.get(size-1); //Last digit from input
        System.out.println("Input: " + numbers);
        System.out.println("Provided: " + provided);
        int sum = luhn.findCheckDigit(numbers); //Correct last digit
        System.out.println("Expected: " + sum);
        System.out.println("\nChecksum: " + luhn.valid(provided, sum));
        System.out.println("Digits: " + luhn.isCreditCard(numbers));
    }
}
