import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgorithmTest {
    LuhnAlgorithm luhn = new LuhnAlgorithm();
    List<Integer> digits15 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,0,1,2,3,4,5));
    List<Integer> digits16 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,2));


    @org.junit.jupiter.api.Test
    void findCheckDigit() {
        assertEquals(7, luhn.findCheckDigit(digits15));
        assertEquals(2, luhn.findCheckDigit(digits16));

    }

    @org.junit.jupiter.api.Test
    void isCreditCard() {
        assertEquals(15+"Not Credit card", luhn.isCreditCard(digits15));
        assertEquals("16 (Credit card)", luhn.isCreditCard(digits16));
    }

    @org.junit.jupiter.api.Test
    void valid() {
        assertEquals("Valid",luhn.valid(2,2));
        assertEquals("Invalid",luhn.valid(1,2));
    }

    @org.junit.jupiter.api.Test
    void creditCardString() {
        assertEquals("(Credit card)",luhn.creditCardString(true));
        assertEquals("Not Credit card",luhn.creditCardString(false));

    }

}