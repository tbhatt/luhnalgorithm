import java.util.List;

public class LuhnAlgorithm {
    public LuhnAlgorithm(){

    }
    //Algorithm to find check digit
    public int findCheckDigit(List<Integer> numbers){
        int sum = 0; //Sum of digit
        boolean alt = true; //Used to multiply every other digit
        int size = numbers.size();
        //Algorithm
        for (int i = size-2; i>=0; i--){
            int digit =  numbers.get(i);
            if(alt){
                digit *= 2;
                if(digit > 9) {
                    digit = digit - 9;
                }
            }
            sum += digit;
            alt = !alt;
        }
        return (sum*9)%10;
    }
    //Check if we have 16 digits
    public String isCreditCard(List<Integer> numbers){
        if (numbers.size() == 16) return "16 "+ creditCardString(true);
        return (numbers.size() + creditCardString(false));

    }

    public String creditCardString(Boolean is){
        if (is) return "(Credit card)";
        return ("Not Credit card");

    }

    //Check if provided and expected check digit are the same.
    public String valid(int a, int b){
        if(a==b) return "Valid";
        return "Invalid";
    }

}
