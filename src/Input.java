import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Input {
    Scanner sc;
    public Input(){
        sc = new Scanner(System.in);
    }

    public List<Integer> getAlgorithm(){
        System.out.println("Please type in digits: ");
        String input;
        if(sc.hasNext()){
            input = sc.nextLine();
            return convertToArray((input));
        }
        return null;
    }

    public List<Integer> convertToArray(String numbers){
        List<Integer> digits = new ArrayList<>();
        for (int i=0; i< numbers.length(); i++){
            char ch = numbers.charAt(i);
            int digit = ch - '0';
            digits.add(digit);
        }
        return digits;
    }
}
